// ==UserScript==
// @name            OPR circles
// @version         0.0.1
// @description     Draw circles
// @homepageURL     https://gitlab.com/pwiecz/opr-circles
// @author          1110101, https://gitlab.com/pwiecz/opr-circles/graphs/master
// @match           https://opr.ingress.com/recon
// @grant           unsafeWindow
// @grant           GM_addStyle
// @downloadURL     https://gitlab.com/pwiecz/opr-circles/raw/master/opr-circles.user.js
// @updateURL       https://gitlab.com/pwiecz/opr-circles/raw/master/opr-circles.user.js
// @supportURL      https://gitlab.com/pwiecz/opr-circles/issues

// ==/UserScript==

/*
  MIT License

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

*/

// polyfill for ViolentMonkey
if (typeof exportFunction !== "function") {
    exportFunction = (func, scope, options) => {
	if (options && options.defineAs) {
	    scope[options.defineAs] = func;
	}
	return func;
    };
}

function init() {
    const w = typeof unsafeWindow == "undefined" ? window : unsafeWindow;
    let tryNumber = 15;


    function initAngular() {
	const el = w.document.querySelector("[ng-app='portalApp']");
	w.$app = w.angular.element(el);
	w.$injector = w.$app.injector();
	w.$rootScope = w.$app.scope();

	w.$scope = element => w.angular.element(element).scope();
    }

    const initWatcher = setInterval(() => {
	if (tryNumber === 0) {
	    clearInterval(initWatcher);
	    return;
	}
	if (w.angular) {
	    let err = false;
	    try {
		initAngular();
	    }
	    catch (error) {
		console.log(error);
		err = error;
	    }
	    if (!err) {
		try {
		    initScript();
		    clearInterval(initWatcher);
		} catch (error) {
		    console.log(error);
		    if (error !== 42) {
			clearInterval(initWatcher);
		    }
		}
	    }
	}
	tryNumber--;
    }, 1000);

    function initScript() {
	const subMissionDiv = w.document.getElementById("NewSubmissionController");
	const subController = w.$scope(subMissionDiv).subCtrl;

	if (subController.reviewType === "NEW") {
	    modifyNewPage(subController);
	}
    }

    function modifyNewPage(subController) {
	let _initMap = subController.initMap;
	subController.initMap = exportFunction(() => {
	    _initMap();
	    addCircles(subController.markers, subController.map2);
	});
	addCircles(subController.markers, subController.map2);
	modifyNewPage = () => {}; // just run once
    }

    function addCircles(markers, map) {
	for (let i = 0; i < markers.length; ++i) {
	    const marker = markers[i];
	    const circle = new google.maps.Circle({
		map          : map,
		center       : marker.position,
		radius       : 20,
		strokeColor  : "red",
		strokeOpacity: 0.8,
		strokeWeight : 1,
		fillColor    : "red",
		fillOpacity  : 0.2,
	    });
	}
    }
}

setTimeout(() => {
    init();
}, 500);
